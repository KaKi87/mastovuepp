if(process.env.NODE_ENV === 'development'){
    globalThis.__VUE_OPTIONS_API__ = true
    globalThis.__VUE_PROD_DEVTOOLS__ = false;
}

import { createApp } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';
import { createPinia, defineStore } from 'pinia';
import piniaPluginPersistedState from 'pinia-plugin-persistedstate';

import PrimeVue from 'primevue/config';
import PrimeVueToastService from 'primevue/toastservice';
import PrimeVueConfirmationService from 'primevue/confirmationservice';
import PrimeVueTooltip from 'primevue/tooltip';
import 'primevue/resources/primevue.min.css';
import 'primevue/resources/themes/mdc-dark-deeppurple/theme.css';
import 'primeicons/primeicons.css';

import localforage from 'localforage';

import createList from './lib/createList.js';

import App from './components/App.vue';
import List from './components/List.vue';
import User from './components/User.vue';
import Instance from './components/Instance.vue';
import InstanceChooser from './components/InstanceChooser.vue';
import Settings from './components/Settings.vue';

const
    app = createApp(App),
    pinia = createPinia();


pinia.use(piniaPluginPersistedState);
app.use(pinia);

const store = defineStore(
    'mastovuepp',
    {
        state: () => ({
            lists: {},
            lastActiveListId: undefined,
            clients: [
                {
                    hostname: 'main.elk.zone',
                    type: 'elk',
                    isDefault: true
                }
            ],
            user: {
                isPosts: true,
                isReplies: true,
                isBoosts: true,
                isIncludeAttachments: false,
                isExcludeAttachments: false
            }
        }),
        persist: true
    }
)();

if(Object.keys(store.lists).length === 0){
    const {
        id,
        options
    } = createList();
    store.lists[id] = options;
    store.lastActiveListId = id;
}

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            redirect: () => ({
                name: 'list',
                params: {
                    'id': store.lastActiveListId
                }
            })
        },
        {
            path: '/list/:id',
            name: 'list',
            component: List
        },
        {
            path: '/user/:tag?',
            name: 'user',
            component: User
        },
        {
            path: '/instance/:hostname?',
            name: 'instance',
            component: Instance
        },
        {
            path: '/instanceChooser',
            name: 'instanceChooser',
            component: InstanceChooser
        },
        {
            path: '/settings',
            name: 'settings',
            component: Settings
        }
    ]
});

localforage.setDriver(localforage.INDEXEDDB);

app.use(router);

app.use(PrimeVue);
app.use(PrimeVueToastService);
app.use(PrimeVueConfirmationService);

app.directive('tooltip', PrimeVueTooltip);

app.config.globalProperties.store = store;
app.config.globalProperties.localforage = localforage;

app.mount('.App');