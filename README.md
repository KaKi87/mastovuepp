# [MastoVue++](https://mastovuepp.kaki87.net)

An advanced Mastodon feed explorer inspired by [MastoVue](https://mastovue.glitch.me).

## Features

- Lists
  - Browse local feed of (m)any instance(s) ([`mastodon/mastodon#6942`](https://github.com/mastodon/mastodon/issues/6942))
    - Workaround timeline-less instances
  - Browse hashtag feed of (m)any instance(s)
  - Browse local feed of (m)any user(s)
  - Filter posts by text/username/language/attachments
- User
  - Browse local feed of a user
  - Filter posts by type (self/reply/boost) & attachments ([`mastodon/mastodon#10011`](https://github.com/mastodon/mastodon/issues/10011))
- Open posts in Elk or post's client
- Import/export settings

## Screenshots

| ![](./screenshots/screenshot1.png) | ![](./screenshots/screenshot2.png) |
|------------------------------------|------------------------------------|
