import axios from 'axios';

const cacheKey = 'mastovuepp.userId';

export default async ({
    hostname,
    username
}) => {
    const userIdCache = JSON.parse(localStorage.getItem(cacheKey) || '{}');
    let userId = userIdCache[hostname]?.[username];
    if(!userId){
        ({
            data: {
                'id': userId
            }
        } = await axios({
            url: `https://${hostname}/api/v1/accounts/lookup`,
            params: {
                'acct': username
            }
        }));
        localStorage.setItem(cacheKey, JSON.stringify({
            ...userIdCache,
            [hostname]: {
                ...userIdCache[hostname],
                [username]: userId
            }
        }));
    }
    return userId;
};