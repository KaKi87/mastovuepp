export default string => {
    const { documentElement } = new DOMParser().parseFromString(string, 'text/html');
    documentElement.querySelectorAll('body > *:not(:last-child)').forEach(paragraphElement => paragraphElement.outerHTML += '\n');
    documentElement.querySelectorAll('br').forEach(newlineElement => newlineElement.outerHTML = '\n');
    return documentElement.textContent;
};