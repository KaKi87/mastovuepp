import dayjs from './dayjs.js';
import stripHtml from './stripHtml.js';

export default item => {
    const isBoost = !!item['reblog'];
    if(isBoost) item = item['reblog'];
    const
        url = new URL(item['url']),
        username = item['account']['username'],
        date = dayjs(item['created_at']),
        isReply = !isBoost && !!item['in_reply_to_id'];
    return {
        url,
        username,
        avatarUrl: item['account']['avatar_static'],
        name: item['account']['display_name'] || username,
        tag: `@${username}@${url.hostname}`,
        language: item['language'],
        content: stripHtml(item['content']),
        date,
        dateTimestamp: date.valueOf(),
        dateAbsoluteText: date.format('MMMM D, YYYY h:mm:ss A'),
        isPost: !isReply && !isBoost,
        isReply,
        isBoost,
        thumbnailUrl: item['media_attachments'][0]?.['preview_url']
    };
};