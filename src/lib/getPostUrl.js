export default ({
    url,
    clientType,
    clientHostname
}) => {
    const
        {
            hostname
        } = url,
        [
            , localTag
            , id
        ] = url.pathname.split('/');
    let clientUrl;
    switch(clientType){
        case 'elk': {
            clientUrl = `https://${clientHostname}/${hostname}/${localTag}/${id}`;
            break;
        }
    }
    return clientUrl;
};