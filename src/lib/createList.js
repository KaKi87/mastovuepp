import { nanoid } from 'nanoid';

export default () => ({
    id: nanoid(),
    options: {
        name: 'Unnamed list',
        scrollTop: 0,
        includeHostnames: [],
        includeHashtag: '',
        includeUsers: [],
        includeText: [],
        excludeText: [],
        excludeUsernames: [],
        includeLanguages: [],
        excludeLanguages: [],
        isIncludeAttachments: false,
        isExcludeAttachments: false
    }
});