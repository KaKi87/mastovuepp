import dayjs from 'dayjs';
import dayjsIsToday from 'dayjs/plugin/isToday';
import dayjsRelativeTime from 'dayjs/plugin/relativeTime';

dayjs.extend(dayjsIsToday);
dayjs.extend(dayjsRelativeTime);

export default dayjs;