import axios from 'axios';

import dayjs from './dayjs.js';
import getUserId from './getUserId.js';

const
    defaultPostFilter = item => dayjs(item['created_at']).isToday(),
    defaultUserFilter = item => dayjs(item['last_status_at']).isToday();

export const
    getUserRawFeed = async ({
        user,
        lastPostId,
        postFilter,
        onProgress
    }) => {
        if(!user.id)
            user.id = user.id = await getUserId(user);
        const rawFeed = [];
        let pageCount = 0;
        do {
            const {
                data
            } = await axios({
                url: `https://${user.hostname}/api/v1/accounts/${user.id}/statuses`,
                params: {
                    'limit': '40',
                    'max_id': lastPostId
                }
            });
            const pageRawFeed = postFilter ? data.filter(postFilter) : data;
            rawFeed.push(...pageRawFeed);
            lastPostId = pageRawFeed.length === 40 ? pageRawFeed.slice(-1)[0]['id'] : undefined;
            if(onProgress?.({
                user,
                pageCount: ++pageCount,
                postCount: rawFeed.length
            })) break;
        }
        while(lastPostId);
        return {
            rawFeed
        };
    },
    getUsersRawFeed = async ({
        users,
        onStart,
        onProgress,
        onSuccess,
        onError
    }) => {
        const rawFeed = [];
        for(const user of users){
            onStart?.({ user });
            try {
                rawFeed.push(...(await getUserRawFeed({
                    user,
                    postFilter: defaultPostFilter,
                    onProgress
                })).rawFeed);
                onSuccess?.({ user });
            }
            catch {
                onError({ user });
            }
        }
        return {
            rawFeed
        };
    },
    getInstancePublicRawFeed = async ({
        hostname,
        lastPostId,
        postFilter,
        onProgress
    }) => {
        const rawFeed = [];
        let
            status,
            pageCount = 0;
        do {
            let data;
            ({
                status,
                data
            } = await axios({
                url: `https://${hostname}/api/v1/timelines/public`,
                params: {
                    'local': 'true',
                    'limit': '40',
                    'max_id': lastPostId
                },
                validateStatus: status => [200, 422].includes(status),
                timeout: status ? undefined : 10000
            }));
            if(status === 200){
                const pageRawFeed = postFilter ? data.filter(postFilter) : data;
                rawFeed.push(...pageRawFeed);
                lastPostId = pageRawFeed.length === 40 ? pageRawFeed.slice(-1)[0]['id'] : undefined;
                if(onProgress?.({
                    hostname,
                    pageCount: ++pageCount,
                    postCount: rawFeed.length
                })) break;
            }
        }
        while(lastPostId);
        return {
            rawFeed: status === 200 ? rawFeed : undefined
        };
    },
    getInstancePrivateRawFeed = async ({
        hostname,
        userFilter,
        postFilter,
        onProgress
    }) => {
        const
            users = [],
            rawFeed = [];
        {
            let offset = 0;
            do {
                const {
                    data
                } = await axios({
                    url: `https://${hostname}/api/v1/directory`,
                    params: {
                        'local': 'true',
                        'limit': '80',
                        offset
                    }
                });
                const pageUsers = userFilter ? data.filter(userFilter) : data;
                users.push(...pageUsers);
                offset = pageUsers.length === 80 ? offset += 80 : undefined;
            }
            while(offset);
        }
        for(let userIndex = 0; userIndex < users.length; userIndex++){
            rawFeed.push(...(await getUserRawFeed({
                user: {
                    hostname,
                    ...users[userIndex]
                },
                postFilter,
                onProgress
            })).rawFeed);
            if(onProgress?.({
                hostname,
                progress: (userIndex + 1) / users.length
            })) break;
        }
        return {
            rawFeed
        };
    },
    getInstanceRawFeed = async ({
        hostname,
        onProgress,
        postFilter,
        userFilter
    }) => ({
        rawFeed:
            (await getInstancePublicRawFeed({
                hostname,
                postFilter,
                onProgress
            })).rawFeed
            ||
            (await getInstancePrivateRawFeed({
                hostname,
                userFilter,
                postFilter,
                onProgress
            })).rawFeed
    }),
    getInstancesRawFeed = async ({
        hostnames,
        onStart,
        onProgress,
        onSuccess,
        onError
    }) => {
        const rawFeed = [];
        for(const hostname of hostnames){
            onStart?.({ hostname });
            try {
                rawFeed.push(...(await getInstanceRawFeed({
                    hostname,
                    onProgress,
                    postFilter: defaultPostFilter,
                    userFilter: defaultUserFilter
                })).rawFeed);
                onSuccess?.({ hostname });
            }
            catch {
                onError({ hostname });
            }
        }
        return {
            rawFeed
        };
    };