import { createServer } from 'vite';
import createVuePlugin from '@vitejs/plugin-vue';
import createRewriteAllPlugin from 'vite-plugin-rewrite-all';

(async () => {
    const server = await createServer({
        root: './src',
        plugins: [
            createVuePlugin(),
            createRewriteAllPlugin()
        ]
    });
    console.log(`Server listening on port ${(await server.listen()).config.server.port}`);
})().catch(console.error);