export default ({
    url,
    clientType,
    clientHostname
}) => {
    const
        {
            hostname
        } = url,
        [
            , localTag
        ] = url.pathname.split('/');
    let clientUrl;
    switch(clientType){
        case 'elk': {
            clientUrl = `https://${clientHostname}/${hostname}/${localTag}`;
            break;
        }
    }
    return clientUrl;
};